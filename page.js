       /*
        var hotel_id = 83722;
        var commenttotal = 4;
        ajax_comment_list(hotel_id,commenttotal,1,'');
        function ajax_comment_list(hotel_id,commenttotal,page,comment_cur){
            var param = {
                hotel_id:hotel_id,
                page:page,
                comment_cur:comment_cur
            };
            var template_good = '<dl><dt><i class="good"></i>推荐</dt><dd><i class="less">[content]</i><span><em>[member]</em>[date]</span></dd></dl>';
            var templdate_bad = '<dl><dt><i class="bad"></i>不推荐</dt><dd><i class="less">[content]</i><span><em>[member]</em>[date]</span></dd></dl>';
            var comment = '';
            var comment_html= '<div class="child-noborder">';
            $.getJSON('/ajax/comment_list.php',param,function(data){
                $.each(data,function(i,comment){
                    if(comment.flag==1){
                        comment_html +=template_good.replace('[content]',comment.content).
                        replace('[member]',comment.nickname).
                        replace('[date]',comment.dateline);
                    }else{
                        comment_html +=templdate_bad.replace('[content]',comment.content).
                        replace('[member]',comment.nickname).
                        replace('[date]',comment.dateline);
                    }
                });
                comment_html +='<ul class="page new_page" id="comment_page"></ul></div>';
                $('#js_review_con').html(comment_html);
                $('#comment_page').html(multi(commenttotal,10,page,'',50));
                $('#comment_page').prev("dl").addClass('noborder');
            });
        }

        $('#pinglun').delegate('#comment_page a','click',function(){
            var click_page =$(this).data('page');
            var comment_cur  = $('#js_review_tag .on').data('comment_cur');
            var data_total = $('#js_review_tag .on').data('total');
            ajax_comment_list(hotel_id,data_total,click_page,comment_cur);
        });
        $('.review_tag_con').delegate('#js_review_tag a','click',function(){
            var comment_cur = $(this).data('comment_cur');
            var data_total = $('#js_review_tag .on').data('total');
            ajax_comment_list(hotel_id,data_total,1,comment_cur);
        });*/
        /* 分页 */
        function multi(num, perpage,curpage,urlf,maxpages) {
            var multipage = '';
            if (num > perpage) {
                var page = 9;
                var offset = 2;
                var realpages = Math.ceil(num /perpage);
                var pages = maxpages && maxpages < realpages ? maxpages : realpages;

                if (page > pages) {
                    var from = 1;
                    var to = pages;
                } else {
                    from = curpage -offset;
                    to = from + page - 1;
                    if (from < 1) {
                        to = curpage + 1 -from;
                        from = 1;
                        if (to - from<page) {
                            to = page;
                        }
                    } else if (to > pages) {
                        from = pages - page + 1;
                        to = pages;
                    }
                }
                multipage += (curpage > 1 ? '<li class="first"><a data-page="'+(curpage-1)+'">上一页</a></li>' : '<li class="first">上一页</li>');

                for (var i = from; i <= to; i++) {
                    multipage += i == curpage ? '<li class="cur"><a data-page="'+i+'">'+i+'</a></li>' :'<li class="num"><a  data-page="'+i+'">'+i+'</a></li>';
                }
                multipage += (curpage <pages ? '<li class="next"><a data-page="'+(curpage + 1)+'">下一页</a></li>' : '');
            }
            return multipage;
        }